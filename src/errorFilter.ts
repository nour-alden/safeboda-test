import {
  ExceptionFilter,
  Catch,
  HttpException,
  ArgumentsHost,
  HttpStatus,
  InternalServerErrorException,
} from '@nestjs/common';
import { logger } from './common';

//Todo add sentry or any other tool to monitoring server
@Catch()
export class ErrorFilter implements ExceptionFilter {
  catch(error: Error & HttpException, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    const status =
      error instanceof HttpException
        ? error.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    const temp: any =
      error instanceof HttpException
        ? error.getResponse()
        : new InternalServerErrorException({});

    if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
      logger.error(response.req.route.path);
      logger.error(error.message);
      logger.error('-'.repeat(50));
    }

    return response.status(status).json(temp);
  }
}
