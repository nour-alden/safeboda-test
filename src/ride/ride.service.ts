import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { Prisma, Ride } from '@prisma/client';
import { PrismaService } from '../prisma.service';

import { PassengerErrors } from '../passenger/passenger.error';
import { DriverErrors } from '../driver/driver.error';

import { RideLocationDto } from './dto';
import { RideStatusEnum } from './enums';
import { RideError } from './ride.error';

@Injectable()
export class RideService {
  constructor(private readonly prisma: PrismaService) {}

  async ride(
    rideWhereUniqueInput: Prisma.RideWhereUniqueInput,
  ): Promise<Ride | null> {
    return this.prisma.ride.findUnique({
      where: rideWhereUniqueInput,
    });
  }

  async rides(params: {
    skip?: number;
    take?: number;
    where?: Prisma.RideWhereInput;
    orderBy?: Prisma.RideOrderByWithRelationInput;
  }): Promise<Ride[]> {
    const { skip, take, where, orderBy } = params;
    return this.prisma.ride.findMany({
      skip,
      take,
      where,
      orderBy,
    });
  }

  async createRide(data: Prisma.RideCreateInput): Promise<Ride> {
    return this.prisma.ride.create({
      data,
    });
  }

  async updateRide(params: {
    where: Prisma.RideWhereUniqueInput;
    data: Prisma.RideUpdateInput;
  }): Promise<Ride> {
    const { where, data } = params;
    return this.prisma.ride.update({
      data,
      where,
    });
  }

  // Not used yet
  async deleteRide(
    where: Prisma.RideWhereUniqueInput,
  ): Promise<Ride | BadRequestException> {
    return this.prisma.ride.delete({
      where,
    });
  }

  async createRideForDriverAndPassenger(
    driverId: number,
    passengerId: number,
    location: RideLocationDto,
  ) {
    const driver = await this.prisma.driver.findUnique({
      where: { id: driverId },
    });
    if (!driver) {
      throw new NotFoundException(DriverErrors.notFound);
    }
    if (driver.suspended) {
      throw new BadRequestException(DriverErrors.suspended);
    }
    const passenger = await this.prisma.passenger.findUnique({
      where: {
        id: passengerId,
      },
    });
    if (!passenger) {
      throw new NotFoundException(PassengerErrors.notFound);
    }

    const ride = await this.prisma.ride.findFirst({
      where: {
        OR: [
          {
            passenger: { id: passengerId },
            status: RideStatusEnum.ONGOING,
          },
          {
            driver: { id: driverId },
            status: RideStatusEnum.ONGOING,
          },
        ],
      },
    });
    if (ride) {
      if (ride.driverId === driverId) {
        throw new BadRequestException(RideError.driverNotAvailable);
      }
      if (ride.passengerId === passengerId) {
        throw new BadRequestException(RideError.passengerNotAvailable);
      }
    }
    const { id, status } = await this.createRide({
      driver: { connect: { id: driverId } },
      passenger: { connect: { id: passengerId } },
      ...location,
    });
    return { passengerId, driverId, id, status };
  }

  async stopRide(id: number) {
    const ride = await this.ride({ id });
    if (!ride) {
      throw new NotFoundException(RideError.notFound);
    }
    if (ride.status === RideStatusEnum.STOP) {
      throw new BadRequestException(RideError.alreadyStop);
    }
    const { driverId, passengerId, status } = await this.updateRide({
      where: { id },
      data: { status: RideStatusEnum.STOP },
    });
    return { driverId, passengerId, status, id };
  }
}
