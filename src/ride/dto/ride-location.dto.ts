import { IsNumber } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
export class RideLocationDto {
  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  latPickup: number;
  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  longPickup: number;
  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  latDestination: number;
  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  longDestination: number;
}
