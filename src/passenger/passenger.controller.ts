import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { PassengerService } from './passenger.service';
import { CreatePassengerDto } from './dto';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Passenger')
@ApiBearerAuth()
@UseGuards(AuthGuard())
@Controller('passenger')
export class PassengerController {
  constructor(private readonly passengerService: PassengerService) {}

  @ApiResponse({ status: 201, description: 'return passenger data' })
  @ApiResponse({ status: 400, description: 'passenger/already-added' })
  @Post()
  create(@Body() createPassengerDto: CreatePassengerDto) {
    return this.passengerService.createPassenger(createPassengerDto);
  }
}
