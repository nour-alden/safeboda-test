export const PassengerErrors = {
  alreadyAdded: {
    message: 'passenger/already-added',
    code: 300,
  },
  notFound: {
    message: 'passenger/not-found',
    code: 301,
  },
};
