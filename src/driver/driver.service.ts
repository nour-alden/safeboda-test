import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { Prisma, Driver } from '@prisma/client';
import { DriverErrors } from './driver.error';
import { RideStatusEnum } from '../ride/enums';

@Injectable()
export class DriverService {
  constructor(private readonly prisma: PrismaService) {}

  async driver(
    driverWhereUniqueInput: Prisma.DriverWhereUniqueInput,
  ): Promise<Driver | null> {
    return this.prisma.driver.findUnique({
      where: driverWhereUniqueInput,
    });
  }

  // Not used yet
  async drivers(params: {
    skip?: number;
    take?: number;
    where?: Prisma.DriverWhereInput;
    orderBy?: Prisma.DriverOrderByWithRelationInput;
  }): Promise<Driver[]> {
    const { skip, take, where, orderBy } = params;
    return this.prisma.driver.findMany({
      skip,
      take,
      where,
      orderBy,
    });
  }

  async createDriver(data: Prisma.DriverCreateInput): Promise<Driver> {
    const driver = await this.driver({ phoneNumber: data.phoneNumber });
    if (driver) {
      throw new BadRequestException(DriverErrors.alreadyAdded);
    }
    return this.prisma.driver.create({
      data,
    });
  }

  async updateDriver(params: {
    where: Prisma.DriverWhereUniqueInput;
    data: Prisma.DriverUpdateInput;
  }): Promise<Driver> {
    const { where, data } = params;
    return this.prisma.driver.update({
      data,
      where,
    });
  }

  async deleteDriver(where: Prisma.DriverWhereUniqueInput): Promise<Driver> {
    return this.prisma.driver.delete({
      where,
    });
  }

  async suspendDriver(id: number) {
    const driver = await this.driver({ id });
    if (!driver) {
      throw new NotFoundException(DriverErrors.notFound);
    }
    const ride = await this.prisma.ride.findFirst({
      where: { driverId: id, status: RideStatusEnum.ONGOING },
    });
    if (ride) {
      throw new BadRequestException(DriverErrors.cannotSuspended);
    }
    if (driver.suspended) {
      throw new BadRequestException(DriverErrors.alreadySuspended);
    }
    await this.updateDriver({ where: { id }, data: { suspended: true } });
  }

  async deleteSuspendDriver(id: number) {
    const driver = await this.driver({ id });
    if (!driver) {
      throw new NotFoundException(DriverErrors.notFound);
    }
    if (!driver.suspended) {
      throw new BadRequestException(DriverErrors.notSuspended);
    }
    await this.deleteDriver({ id });
  }
}
