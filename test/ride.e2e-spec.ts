import { INestApplication } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import initTestAppWithToken from './initTestAppWithToken';
import * as request from 'supertest';
import { DriverErrors } from '../src/driver/driver.error';
import { PassengerErrors } from '../src/passenger/passenger.error';
import { RideStatusEnum } from '../src/ride/enums';
import { RideError } from '../src/ride/ride.error';

describe('RideController (e2e)', () => {
  let app: INestApplication;
  const prisma = new PrismaClient();
  let loggedInToken = '';

  beforeAll(async () => {
    const { testApp, testToken } = await initTestAppWithToken();
    app = testApp;
    loggedInToken = testToken;
  });
  it('create ride without token', () => {
    return request(app.getHttpServer())
      .post('/ride/1/1')
      .send({
        latPickup: '0.0',
        longPickup: '0.0',
        latDestination: '0.0',
        longDestination: '0.0',
      })
      .expect(401)
      .expect({
        statusCode: 401,
        message: 'Unauthorized',
      });
  });
  it('stop ride without token', () => {
    return request(app.getHttpServer())
      .post('/ride/1/stop')
      .expect(401)
      .expect({
        statusCode: 401,
        message: 'Unauthorized',
      });
  });
  it('get ongoing without token', () => {
    return request(app.getHttpServer())
      .get('/ride/ongoing')
      .expect(401)
      .expect({
        statusCode: 401,
        message: 'Unauthorized',
      });
  });
  it('create ride for Suspended driver', async () => {
    const driver = await prisma.driver.findFirst({
      where: { suspended: true },
    });
    const passenger = await prisma.passenger.findFirst({
      where: {},
    });
    return request(app.getHttpServer())
      .post(`/ride/${passenger.id}/${driver.id}`)
      .set('Authorization', `bearer ${loggedInToken}`)

      .send({
        latPickup: '0.0',
        longPickup: '0.0',
        latDestination: '0.0',
        longDestination: '0.0',
      })
      .expect(400)
      .expect(DriverErrors.suspended);
  });
  it('create ride for passenger not found', async () => {
    const driver = await prisma.driver.findFirst({
      where: { suspended: false },
    });
    return request(app.getHttpServer())
      .post(`/ride/-1/${driver.id}`)
      .set('Authorization', `bearer ${loggedInToken}`)

      .send({
        latPickup: '0.0',
        longPickup: '0.0',
        latDestination: '0.0',
        longDestination: '0.0',
      })
      .expect(404)
      .expect(PassengerErrors.notFound);
  });
  it('create ride for driver not found', async () => {
    const passenger = await prisma.passenger.findFirst({
      where: {},
    });
    return request(app.getHttpServer())
      .post(`/ride/${passenger.id}/-1`)
      .set('Authorization', `bearer ${loggedInToken}`)

      .send({
        latPickup: '0.0',
        longPickup: '0.0',
        latDestination: '0.0',
        longDestination: '0.0',
      })
      .expect(404)
      .expect(DriverErrors.notFound);
  });
  it('create ride', async () => {
    const rides = await prisma.ride.findMany({
      where: { status: RideStatusEnum.ONGOING },
    });

    const passenger = await prisma.passenger.findFirst({
      where: { id: { notIn: rides.map((item) => item.passengerId) } },
    });
    const driver = await prisma.driver.findFirst({
      where: {
        id: { notIn: rides.map((item) => item.driverId) },
        suspended: false,
      },
    });

    return request(app.getHttpServer())
      .post(`/ride/${passenger.id}/${driver.id}`)
      .set('Authorization', `bearer ${loggedInToken}`)

      .send({
        latPickup: '0.0',
        longPickup: '0.0',
        latDestination: '0.0',
        longDestination: '0.0',
      })
      .expect(201)
      .expect((res) => {
        expect(res.body.driverId).toBe(driver.id);
        expect(res.body.passengerId).toBe(passenger.id);
        expect(res.body.status).toBe(RideStatusEnum.ONGOING);
      });
  });
  it('create ride Not available driver', async () => {
    const rides = await prisma.ride.findMany({
      where: { status: RideStatusEnum.ONGOING },
    });

    const passenger = await prisma.passenger.findFirst({
      where: { id: { notIn: rides.map((item) => item.passengerId) } },
    });
    const driver = await prisma.driver.findFirst({
      where: {
        id: { in: rides.map((item) => item.driverId) },
        suspended: false,
      },
    });

    return request(app.getHttpServer())
      .post(`/ride/${passenger.id}/${driver.id}`)
      .set('Authorization', `bearer ${loggedInToken}`)

      .send({
        latPickup: '0.0',
        longPickup: '0.0',
        latDestination: '0.0',
        longDestination: '0.0',
      })
      .expect(400)
      .expect(RideError.driverNotAvailable);
  });
  it('create ride Not available passenger', async () => {
    const rides = await prisma.ride.findMany({
      where: { status: RideStatusEnum.ONGOING },
    });

    const passenger = await prisma.passenger.findFirst({
      where: { id: { in: rides.map((item) => item.passengerId) } },
    });
    const driver = await prisma.driver.findFirst({
      where: {
        id: { notIn: rides.map((item) => item.driverId) },
        suspended: false,
      },
    });

    return request(app.getHttpServer())
      .post(`/ride/${passenger.id}/${driver.id}`)
      .set('Authorization', `bearer ${loggedInToken}`)

      .send({
        latPickup: '0.0',
        longPickup: '0.0',
        latDestination: '0.0',
        longDestination: '0.0',
      })
      .expect(400)
      .expect(RideError.passengerNotAvailable);
  });
  it('get ongoing rides', async () => {
    return request(app.getHttpServer())
      .get(`/ride/ongoing`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(200)
      .expect((res) => {
        expect(
          res.body.find((item) => item.staus === RideStatusEnum.STOP),
        ).toBe(undefined);
      });
  });
  it('stop ride not found', async () => {
    return request(app.getHttpServer())
      .post(`/ride/-1/stop`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(404)
      .expect(RideError.notFound);
  });
  it('stop ride already stopped', async () => {
    const ride = await prisma.ride.findFirst({
      where: { status: RideStatusEnum.STOP },
    });
    return request(app.getHttpServer())
      .post(`/ride/${ride.id}/stop`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(400)
      .expect(RideError.alreadyStop);
  });
  it('stop ride', async () => {
    const ride = await prisma.ride.findFirst({
      where: { status: RideStatusEnum.ONGOING },
    });
    return request(app.getHttpServer())
      .post(`/ride/${ride.id}/stop`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(201)
      .expect((res) => {
        expect(res.body.id).toBe(ride.id);

        expect(res.body.driverId).toBe(ride.driverId);
        expect(res.body.passengerId).toBe(ride.passengerId);
        expect(res.body.status).toBe(RideStatusEnum.STOP);
      });
  });
  afterAll(async () => {
    await prisma.ride.deleteMany({});
    await prisma.driver.deleteMany({});
    await prisma.passenger.deleteMany({});

    await app.close();
  });
});
