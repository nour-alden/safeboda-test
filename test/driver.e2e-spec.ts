import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { PrismaClient } from '@prisma/client';
import { DriverErrors } from '../src/driver/driver.error';
import * as faker from 'faker';
import initTestAppWithToken from './initTestAppWithToken';
import { RideStatusEnum } from '../src/ride/enums';

describe('DriverController (e2e)', () => {
  let app: INestApplication;
  const prisma = new PrismaClient();
  let loggedInToken = '';

  beforeAll(async () => {
    const { testApp, testToken } = await initTestAppWithToken();
    app = testApp;
    loggedInToken = testToken;

    Array.from({ length: 100 }, async (_, i) => {
      const data = {
        suspended: i < 50,
        name: faker.name.findName(),
        phoneNumber: faker.phone.phoneNumber(),
      };
      await prisma.driver.create({ data });
    });
  });

  it('create driver without token', () => {
    return request(app.getHttpServer())
      .post('/driver')
      .send({
        name: 'test',
        phoneNumber: '+13253243253',
      })
      .expect(401)
      .expect({
        statusCode: 401,
        message: 'Unauthorized',
      });
  });
  it('suspend driver without token', () => {
    return request(app.getHttpServer())
      .post('/driver/1/suspend')
      .expect(401)
      .expect({
        statusCode: 401,
        message: 'Unauthorized',
      });
  });
  it('delete driver without token', () => {
    return request(app.getHttpServer())
      .delete('/driver/1/suspend')
      .expect(401)
      .expect({
        statusCode: 401,
        message: 'Unauthorized',
      });
  });
  it('create driver', () => {
    return request(app.getHttpServer())
      .post('/driver')
      .set('Authorization', `bearer ${loggedInToken}`)
      .send({
        name: 'test',
        phoneNumber: '+13253243253',
      })
      .expect(async (res) => {
        await expect(res.body.suspended).toBe(false);
        await expect(res.body.name).toBe('test');
        await expect(res.body.phoneNumber).toBe('+13253243253');
      });
  });
  it('create driver already Added', () => {
    return request(app.getHttpServer())
      .post('/driver')
      .set('Authorization', `bearer ${loggedInToken}`)
      .send({
        name: 'test',
        phoneNumber: '+13253243253',
      })
      .expect(400)
      .expect(DriverErrors.alreadyAdded);
  });
  it('Suspended driver not found', () => {
    return request(app.getHttpServer())
      .post('/driver/-1/suspend')
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(404)
      .expect(DriverErrors.notFound);
  });
  it('delete driver not found', () => {
    return request(app.getHttpServer())
      .delete('/driver/-1/suspend')
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(404)
      .expect(DriverErrors.notFound);
  });
  it('Suspended driver already Suspended', async () => {
    const driver = await prisma.driver.findFirst({
      where: { suspended: true },
    });

    return request(app.getHttpServer())
      .post(`/driver/${driver.id}/suspend`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(400)
      .expect(DriverErrors.alreadySuspended);
  });
  it('Suspended driver', async () => {
    const rides = await prisma.ride.findMany({
      where: { status: RideStatusEnum.STOP },
    });
    const driver = await prisma.driver.findFirst({
      where: {
        suspended: false,
        id: { in: rides.map((item) => item.driverId) },
      },
    });

    return request(app.getHttpServer())
      .post(`/driver/${driver.id}/suspend`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(204)
      .expect(async () => {
        const newDriver = await prisma.driver.findUnique({
          where: { id: driver.id },
        });
        expect(newDriver.suspended).toBe(true);
      });
  });

  it('Suspended driver has ongoing ride', async () => {
    const ride = await prisma.ride.findFirst({
      where: { status: RideStatusEnum.ONGOING },
    });

    return request(app.getHttpServer())
      .post(`/driver/${ride.driverId}/suspend`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(400)
      .expect(DriverErrors.cannotSuspended);
  });
  it('Delete driver not Suspended', async () => {
    const driver = await prisma.driver.findFirst({
      where: { suspended: false },
    });
    return request(app.getHttpServer())
      .delete(`/driver/${driver.id}/suspend`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(400)
      .expect(DriverErrors.notSuspended);
  });
  it('Delete driver', async () => {
    const driver = await prisma.driver.findFirst({
      where: { suspended: true },
    });

    return request(app.getHttpServer())
      .delete(`/driver/${driver.id}/suspend`)
      .set('Authorization', `bearer ${loggedInToken}`)
      .expect(204)
      .expect(async () => {
        const newDriver = await prisma.driver.findUnique({
          where: { id: driver.id },
        });
        expect(newDriver).toBe(null);
      });
  });

  afterAll(async () => {
    await prisma.ride.deleteMany({});
    await prisma.driver.deleteMany({});
    await prisma.passenger.deleteMany({});
    await app.close();
  });
});
